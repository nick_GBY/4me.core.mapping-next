import {} from 'dotenv/config';

import fp from 'lodash/fp';
import R from 'ramda';
import { connectToDb, tables } from '../src/rethink';
import r from 'rethinkdb';


const setName = client => {
  const id = fp.get('id', client);
  const name = `P${id}`;

  return Object.assign({}, {name}, client);
}

const clients = fp.pipe(
  fp.getOr([], 'default'),
  fp.map(fp.defaults({type: "cwp"})),
  fp.map(setName),
)(require('../data/clients'));

let i = 1;
const sectors = fp.pipe(
  fp.getOr([], 'default'),
  fp.map(s => fp.defaults({id: i++}, s)),
)(require('../data/sectors'));

async function createTables(conn) {
  const existingTables = await r.tableList().run(conn);

  const checkOrCreateTable = conn => tableName => {
    if(existingTables.includes(tableName)) {
      console.log(`\t${tableName} table already exists`);
      return;
    }

    console.log(`Creating table : ${tableName}`);
    return r.tableCreate(tableName).run(conn);
  };

  return fp.mapValues(checkOrCreateTable(conn), tables);
}

async function insertClients(conn, clients) {
  await r.table(tables.clients).delete().run(conn);
  return await r.table(tables.clients).insert(clients, {conflict: "update"}).run(conn);
}

async function insertSectors(conn, sectors) {
  await r.table(tables.sectors).delete().run(conn);
  return await r.table(tables.sectors).insert(sectors, {conflict: "update"}).run(conn);
}

async function insertMap(conn) {
  // Create index
  const indexes = await r.table(tables.map).indexList().run(conn);

  if(!R.contains('when', indexes)) {
    console.log('Creating index on table map');
    await r.table(tables.map).indexCreate('when').run(conn);
  }

  const client = await r.table(tables.clients)
    .filter(client => client('type').eq('cwp'))
    .run(conn)
    .then(res => res.toArray())
    .then(fp.head);

  const sectors = await r.table(tables.sectors)
    .filter(sector => sector('elementarySectors').count().eq(1))
    .run(conn)
    .then(res => res.toArray())
    .then(fp.pipe(
      fp.map(fp.get('name')),
    ));

  const map = [
    {cwpId: fp.get('id', client), sectors}
  ];

  const rowMap = {
    map,
    when: r.now(),
  };

  // Prune db
  await r.table(tables.map)
    .delete()
    .run(conn);

  return await r.table(tables.map)
    .insert(rowMap)
    .run(conn);
}

(async function() {
  try {
    const conn = await connectToDb();
    console.log('DB OK');

    await createTables(conn);
    console.log(`Tables OK`);

    const clientInserts = await insertClients(conn, clients);
    const sectorInserts = await insertSectors(conn, sectors);
    const mapInserts = await insertMap(conn);

    console.log(`Data OK`);

    process.exit(0);
  } catch (err) {
    console.log(err);
    process.exit(1)
  }
})();


