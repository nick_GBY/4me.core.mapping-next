import {getMapQuery, table, getMapChangeFeed} from './db';
import r from 'rethinkdb';
import R from 'ramda'
import d from 'debug';
const debug = d('4me.map.changes');

import {getCoreClients, getMappingClients} from '../socket';

export function installChangeFeeds(deps = {}) {
  const {database, io} = deps;

  let calls = 0;
  getMapChangeFeed()
    .changes()
    .run(database, (err, cursor) => {
      if(err) {
        throw err;
        return;
      }

      cursor.each((err, data) => {
        const newMap = R.prop('new_val', data);
        debug('Broadcasting new map to clients !');
        const mappingClientsToNotify = getMappingClients();
        broadcastNewMap(mappingClientsToNotify, newMap);

        const coreClientsToNotify = getCoreClients();
        R.forEach(c => c.emit('mapping:refresh'), coreClientsToNotify);
      });
  });
}

function broadcastNewMap(clients, newMap) {
  R.forEach(c => c.emit('map_updated', newMap), clients);
}
