import {tables} from '../rethink';
import R from 'ramda';
import r from 'rethinkdb';

export const table = r.table(tables.map);

export async function getMap(database) {
  return getMapQuery()
    .run(database);
}

import validateMap from './validation';

export async function setMap(database, map = []) {
  const rowMap = {
    map,
    when: r.now(),
  };

  const insertToDb = r.table(tables.map).insert(rowMap);
  // This will throw if the new map is invalid
  await validateMap(database, map);

  return insertToDb.run(database)
    .then(() => garbageCollect(r.table(tables.map)).run(database));
}

export function getMapQuery() {
  return getMapChangeFeed().nth(0);
}

export function garbageCollect(table) {
  return table
    .orderBy({index: r.desc('when')})
    .slice(10)
    .delete();
}

export function getMapChangeFeed() {
  return table
    .orderBy({index: r.desc('when')})
    .getField('map')
    .limit(1);
}

