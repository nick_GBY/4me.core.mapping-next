import {tables} from '../rethink';
import fp from 'lodash/fp';
import R from 'ramda';
import r from 'rethinkdb';

import {installChangeFeeds} from './changes';

export default function installRoutes(router, deps = {}) {

  router.get('/', getMap(deps));
  router.post('/', setMap(deps));

  router.get('/suggest/:cwpId', getSuggestions(deps));
  router.get('/:id', getSingle(deps));

  //router.get('/', ctx => { ctx.body = 'pouet pouet'; });
  // Install live queries
  installChangeFeeds(deps);
  return router;
}

import { getElementarySectors } from '../sectors/db';
import { getCwpClients } from '../clients/db';

import {
  getMap as getMapFromDb,
  setMap as setMapToDb,
} from './db';

export function getMap(deps) {
  const {database} = deps;

  return async (ctx) => {
    try {
      ctx.body = await getMapFromDb(database);
    } catch(err) {
      ctx.throw(500, err);
    }
  };
}

export function setMap(deps) {
  const {database} = deps;

  return async (ctx) => {
    try {
      const newMap = ctx.request.body;
      await setMapToDb(database, newMap);
      ctx.body = newMap;
    } catch(err) {
      ctx.throw(500, err);
    }
  };
}

export function getSingle(deps) {
  const {database} = deps;

  return async ctx => {
    try {
      const {id} = ctx.params;
      if(!id) {
        ctx.throw(404);
        return;
      }
      const map = await getMapFromDb(database);
      const findMapItem = R.pipe(
        R.find(R.propEq('cwpId', parseInt(id, 10))),
        R.merge({cwpId: id, sectors: []}),
      );

      ctx.body = findMapItem(map);

    } catch(err) {
      ctx.throw(500, err);
    }
  }
}

import {findSuggestions} from './suggestion';

export function getSuggestions(deps) {
  const {database} = deps;

  return async (ctx) => {
    try {
      const cwpId = ctx.params.cwpId;

      const suggestions = await findSuggestions(database, cwpId);

      ctx.body = suggestions;
    } catch(err) {
      ctx.throw(500, err);
    }
  }
}
