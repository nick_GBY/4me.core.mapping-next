import R from 'ramda';
import r from 'rethinkdb';

import {
  getById as getClientById,
  getCwpClients,
} from '../clients/db';

import {
  getSectors,
} from '../sectors/db';

import {
  getMap,
} from './db';

export async function findSuggestions(database, rawCwpId) {
  if(!parseInt(rawCwpId, 10)) {
    throw new Error('Invalid argument');
  }

  const cwpId = parseInt(rawCwpId, 10);

  const cwp = await getClientById(database, cwpId);

  if(!cwp || R.prop('type', cwp) !== 'cwp') {
    throw new Error('Invalid CWP');
  }


  const [sectors, clients, map] = await Promise.all([
    getSectors(database),
    getCwpClients(database),
    getMap(database),
  ]);

  const ourSectors = R.pipe(
    R.find(R.propEq('cwpId', cwp.id)),
    R.propOr([], 'sectors'),
  )(map);

  if(R.isEmpty(ourSectors)) {
    return _suggestOnEmptyCwp(database, sectors, clients, map, cwp);
  }

  return _suggestOnBoundCwp(database, sectors, clients, map, cwp);

}

// Takes an array of elementary sectors and return the proper sector definition
const getBySectors = sectorTree => lookupSectors => R.find(
  R.pipe(
    // Extract elementary sectors
    R.propOr([], 'elementarySectors'),
    // Check elements are equals
    R.symmetricDifference(lookupSectors),
    R.isEmpty
  )
)(sectorTree);

// Takes a group name an return the proper sector definition
const getSectorByName = sectorTree => name => R.find(R.propEq('name', name), sectorTree);

async function _suggestOnBoundCwp(database, sectors, clients, map, targetCwp) {
  // TODO : Refactor MAP stuff
  const cwpId = R.prop('id', targetCwp);

  const ourSectors = R.pipe(
    R.find(R.propEq('cwpId', cwpId)),
    R.propOr([], 'sectors'),
  )(map);

  const getAndExpandSuggestions = R.pipe(
    // Expand our bound sectors
    getBySectors(sectors),
    // Find canAccept
    R.propOr([], 'canAccept'),
    R.map(
      // For each group in canAccept
      R.pipe(
        // Expand
        getSectorByName(sectors),
        // Get elementary sectors
        R.propOr([], 'elementarySectors'),
        // Append our sectors
        R.concat(ourSectors),
        // Filter duplicates (should never happen)
        R.uniq,
        // Expand
        getBySectors(sectors),
        // Pluck props
        R.props(['name', 'elementarySectors']),
        // Transform array to object
        ([name, sectors]) => ({name, sectors}),
      )
    ),
  );


  return getAndExpandSuggestions(ourSectors);
}

async function _suggestOnEmptyCwp(database, sectors, clients, map, targetCwp) {
  // Find everything the room can give

  const possibleSectors = R.pipe(
    // Loop through the room
    R.map(
      R.pipe(
        // Get sectors bound to current cwp
        R.propOr([], 'sectors'),
        // Get SectorGroup item
        getBySectors(sectors),
        // Add sectorGroup item to sectorGroup.canGive
        ourSectors => {
          const canGive = R.propOr([], 'canGive', ourSectors);
          const ourElementarySectors = R.propOr(null, 'name', ourSectors);
          return [ourElementarySectors, ...canGive];
        },
        R.reject(R.isNil),
        R.map(getSectorByName(sectors)),
      ),
    ),
    R.flatten,
    R.pluck('elementarySectors'),
  )(map);


  // At this point we have an array of arrays of elementary sectors

  // Find filtering sectors
  const filteringSectors = R.pipe(
    R.pathOr([], ['suggestions', 'filteredSectors']),
    R.map(getSectorByName(sectors)),
    R.map(R.propOr([], 'elementarySectors')),
    R.flatten,
  )(targetCwp);


  const hasSectorInCommon = filteringSectors => R.pipe(
    R.intersection(filteringSectors),
    R.isEmpty,
    R.not,
  );

  // Reject suggestions included in filtering sectors
  const filteredSuggestions = R.reject(hasSectorInCommon(filteringSectors), possibleSectors);

  // Extract and expand preference order
  const preferenceOrder = R.pipe(
    R.pathOr([], ['suggestions', 'preferenceOrder']),
    R.map(getSectorByName(sectors)),
  )(targetCwp);


  // Sort the suggestions
  const sortedSuggestions = R.sortBy(
    R.pipe(
      _rateSuggestion(sectors, preferenceOrder),
      R.negate,
    )
  , filteredSuggestions);

  // Return array of {name: '', sectors: []} objects
  return R.map(
    R.pipe(
      getBySectors(sectors),
      R.props(['name', 'elementarySectors']),
      ([name, sectors]) => ({name, sectors}),
    )
  )(sortedSuggestions);

}

function _rateSuggestion(sectorTree, preferenceOrder) {
  const mapIndex = R.addIndex(R.map);
  const weightTable = mapIndex((val, index, total) => {
    const weight = R.length(total) - index;
    return {
      weight,
      sectors: R.propOr([], 'elementarySectors', val),
    };
  }, preferenceOrder);


  return (sectors) => {

    const findWeight = R.pipe(
      R.find(
        R.pipe(
          // Get sectors from weighted list
          R.propOr([], 'sectors'),
          // Remove them from sectors to rate
          R.without(R.__, sectors),
          // If it's empty, that means our supplied sector is included in current weighted group
          R.isEmpty,
        ),
      ),
      R.propOr(0, 'weight'),
    );

    return findWeight(weightTable);
  }
}
