import fp from 'lodash/fp';
import _ from 'lodash';

import R from 'ramda';

import { getCwpClients } from '../clients/db';
import { getElementarySectors } from '../sectors/db';

export default async function validate(database, newMap) {
  if(!fp.isArray(newMap)) {
    throw new Error('Invalid argument : newMap should be an array');
  }

  return R.pipeP(
    // Check input structure
    checkInputFormat,
    // Check for duplicate sectors
    checkForDuplicateSectors,
    // Check for disabled CWPs
    checkDisabledCwps,
    // Check if all CWP exist
    checkCwpExistence(database),
    // Check for sector existence
    checkSectors(database),
  )(newMap);

  // Check if all sectors exist and are elementary

  // Check if some sectors are missing

  return newMap;
}

function checkDisabledCwps(newMap) {
  const isDisabled = R.propOr(false, 'disabled');
  const getDisabledCwps = R.filter(isDisabled);

  const hasEmptySectors = R.pipe(
    R.propOr([], 'sectors'),
    R.isEmpty
  );

  const getDisabledCwpsWithSectors = R.pipe(
    getDisabledCwps,
    R.reject(hasEmptySectors),
  );

  const hasDisabledCwpsWithSectors = R.pipe(
    getDisabledCwpsWithSectors,
    R.isEmpty,
    R.not,
  );

  if(hasDisabledCwpsWithSectors(newMap)) {
    throw new Error('Disabled CWP with sectors found !');
  }


  return Promise.resolve(newMap);
}

function checkCwpExistence(database) {
  return async newMap => {
    const cwpIds = fp.map('cwpId', newMap);
    const knownCwpIds = fp.map('id', await getCwpClients(database));

    const getUnknownCwpIds = fp.pipe(
      fp.map('cwpId'),
      fp.without(knownCwpIds),
    );

    const hasUnknownCwpIds = fp.pipe(
      getUnknownCwpIds,
      fp.isEmpty,
      bool => !bool,
    );

    if(hasUnknownCwpIds(newMap)) {
      throw new Error('Invalid cwpId !');
    }

    return newMap;
  };
}

const getReferencedSectors = fp.pipe(
  R.map(R.prop('sectors')),
  R.filter(Boolean),
  R.flatten,
);

function checkSectors(database) {
  return async newMap => {
    const getKnownSectors = R.pipe(
      R.map(R.prop('elementarySectors')),
      R.filter(Boolean),
      R.flatten,
    );

    const knownSectors = getKnownSectors(await getElementarySectors(database));


    const getUnknownSectors = R.pipe(
      getReferencedSectors,
      R.without(knownSectors),
    );

    const hasUnknownSectors = R.pipe(
      getUnknownSectors,
      R.isEmpty,
      R.not,
    );

    if(hasUnknownSectors(newMap)) {
      throw new Error('Unknown sectors found');
    }

    const getMissingSectors = R.pipe(
      getReferencedSectors,
      R.without(R.__, knownSectors),
    );

    const hasMissingSectors = R.pipe(
      getMissingSectors,
      R.isEmpty,
      R.not,
    );

    if(hasMissingSectors(newMap)) {
      throw new Error('Missing sectors found');
    }

    return newMap;
  };
}

function checkInputFormat(newMap) {
  if(!fp.isArray(newMap)) {
    throw new Error('Invalid argument : newMap should be an array');
  }

  const checkFormat = mapItem => {
    const hasNumberCwpId = fp.pipe(
      fp.get('cwpId'),
      fp.isNumber,
    );

    const hasArrayOfSectors = fp.pipe(
      fp.get('sectors'),
      fp.isArray,
    );

    const hasEmptySectors = fp.pipe(
      fp.get('sectors'),
      fp.isEmpty,
    );

    const isDisabled = fp.pipe(
      fp.get('disabled'),
      disabled => !!disabled,
    );

    if(!hasNumberCwpId(mapItem)) {
      throw new Error('Invalid argument : cwpId must be a number');
    }

    if(!hasArrayOfSectors(mapItem) && !isDisabled(mapItem)) {
      throw new Error('Invalid argument : empty data');
    }

    return mapItem;
  }

  return Promise.resolve(fp.map(checkFormat, newMap));
}

function checkForDuplicateSectors(newMap) {
  const hasDuplicateSectors = newMap => {
    return fp.pipe(getReferencedSectors, fp.size)(newMap) !== fp.pipe(getReferencedSectors, fp.uniq, fp.size)(newMap)
  }

  if(hasDuplicateSectors(newMap)) {
    throw new Error('Duplicate sectors found !');
  }

  return Promise.resolve(newMap);
}
