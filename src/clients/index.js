import {tables} from '../rethink';
import fp from 'lodash/fp';
import r from 'rethinkdb';


if(!fp.get('clients', tables)) {
  throw new Error('WOOPS !');
}

export default function installRoutes(router, deps = {}) {

  router.get('/', getClients(deps));
  router.get('/me', identify(deps));
  router.get('/:id', getSingle(deps));
  //router.get('/', ctx => { ctx.body = 'pouet pouet'; });
  return router;
}

export function getSingle(deps) {
  const {database} = deps;

  return async ctx => {

    const {id} = ctx.params;
    try {
      const result = await r.table(tables.clients)
        .get(parseInt(id, 10))
        .run(database);

      if(!result) {
        ctx.throw(404, new Error('Not found'));
      }

      ctx.body = result;
    } catch(err) {
      ctx.throw(500, err);
    }
  }
}

export function getClients(deps) {
  const {database} = deps;

  return async (ctx) => {
    try {
      ctx.body = await r
        .table(tables.clients)
        .run(database)
        .then(res => res.toArray());
    } catch(err) {
      ctx.throw(500, err);
    }
  };
}

export function identify(deps) {
  const {database} = deps;

  return async (ctx) => {
    try {
      ctx.body = await r
        .table(tables.clients)
        .get(30)
        .run(database);
    } catch(err) {
      ctx.throw(500, err);
    }
  }
}
