import {tables} from '../rethink';
import fp from 'lodash/fp';
import r from 'rethinkdb';

export function getClientsQuery(filter = () => true) {
  return r
    .table(tables.clients)
    .filter(filter);
}

export function getCwpClientsQuery() {
  const cwpClientFilter = client => client('type').eq('cwp');
  return getClientsQuery(cwpClientFilter);
}

export async function getClients(database, filter = () => true) {
  return getClientsQuery()
    .run(database)
    .then(res => res.toArray());
}

export async function getById(database, id) {
  return r
    .table(tables.clients)
    .get(id)
    .run(database);
}

export async function getCwpClients(database) {
  return getCwpClientsQuery()
    .run(database)
    .then(res => res.toArray());
}
