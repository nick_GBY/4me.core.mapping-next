import R from 'ramda';
import d from 'debug';
const debug = d('4me.socket');

let coreClients = [];
let mappingClients = [];

export function attachHandlers(socket) {
  socket.on('connect', socket => {
    const cwpId = parseInt(R.path(['handshake', 'query', 'cwp-id'], socket), 10);
    const ipAddress = R.path(['request', 'headers', 'x-forwarded-for'], socket) || R.path(['request', 'connection', 'remoteAddress'], socket);
    if(cwpId) {
      debug(`Core client connected : ${ipAddress} / CWP#${cwpId} / ${socket.id}`);
      socket.cwpId = cwpId;
      coreClients = [...coreClients, socket];
    } else {
      debug(`Mapping client connected : ${ipAddress} / ${socket.id}`);
      mappingClients = [...mappingClients, socket];
    }

    socket.on('disconnect', () => {
      if(socket.cwpId) {
        debug(`Core client disconnected : ${ipAddress} / CWP#${socket.cwpId} / ${socket.id}`);
        coreClients = R.without(socket, coreClients);
      } else {
        debug(`Mapping client disconnected : ${ipAddress} / ${socket.id}`);
        mappingClients = R.without(socket, mappingClients);
      }
    });
  })
}

export function getCoreClients() {
  return coreClients;
}

export function getMappingClients() {
  return mappingClients;
}
