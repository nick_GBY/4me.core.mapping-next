import d from 'debug';
const debug = d('4me.rethink');
import r from 'rethinkdb';
import fp from 'lodash/fp';

export async function connectToDb() {


  let dbHandler;

  try {
    dbHandler = await r.connect({
      host: process.env.RETHINK_HOST || 'localhost',
      port: parseInt(process.env.RETHINK_PORT) || 28015,
      db: process.env.RETHINK_DATABASE || '4me',
    });
  } catch(err) {
    debug('Could not connect to database !');
    debug(err);
    process.exit(1);
  }

  debug('Connected to database');

  return dbHandler;
}

const envTables = {
  // Key is the exported table shortcut
  // Value is the relevant key in process.env
  clients: 'RETHINK_CLIENT_TABLE',
  sectors: 'RETHINK_SECTOR_TABLE',
  map: 'RETHINK_MAP_TABLE',
};

const transformTableConf = fp.mapValues(value => {

  if(!process.env[value]) {
    throw new Error(`${value} is not defined in env !`);
  }

  return process.env[value];
});


export const tables = transformTableConf(envTables);
