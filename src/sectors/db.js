import {tables} from '../rethink';
import fp from 'lodash/fp';
import r from 'rethinkdb';
import R from 'ramda';

export async function getSectors(database, filter = () => true) {
  return r
    .table(tables.sectors)
    .filter(filter)
    .run(database)
    .then(res => res.toArray());
}

export async function getBySectors(database, sectors = []) {
  const findByElementarySectors = R.pipe(
    R.propOr([], 'elementarySectors'),
    R.symmetricDifference(sectors),
    R.isEmpty,
  );

  return r.table(tables.sectors)
    .merge(sectorRow => {
      const canAccept = sectorRow('canAccept').default([])
        .map(canAccept => r.table(tables.sectors)
          .filter({name: canAccept})
          .nth(0)
          .pluck('elementarySectors', 'name')
          .merge(row => ({sectors: row('elementarySectors')}))
          .without('elementarySectors')
          .default(null)
        );

      return {canAccept};
    })
    .run(database)
    .then(res => res.toArray())
    .then(R.pipe(
      R.find(findByElementarySectors),
      R.defaultTo({}),
    ));
}

export function getBySectorsQuery(sectors = []) {
  return r.table(tables.sectors)
    .filter(sectorRow =>
      sectorRow('elementarySectors').default([]).setDifference(sectors).isEmpty() &&
      r.expr(sectors).setDifference(sectorRow('elementarySectors').default([])).isEmpty()
    )
    .merge(sectorRow => {
      // Expand canAccept field
      const expandedCanAccept = sectorRow('canAccept').default([])
        .map(canAccept => r.table(tables.sectors)
          .filter({name: canAccept})
          .nth(0)
          .getField('elementarySectors')
          .default(null)
        );
      return {expandedCanAccept};
    })
    .nth(0)
    .default({});
}

export async function getElementarySectors(database) {
  const elementarySectorFilter = sector => sector('elementarySectors').count().eq(1);
  return getSectors(database, elementarySectorFilter);
}
