import {tables} from '../rethink';
import fp from 'lodash/fp';
import r from 'rethinkdb';


if(!fp.get('sectors', tables)) {
  throw new Error('WOOPS !');
}

export default function installRoutes(router, deps = {}) {

  router.get('/', getSectors(deps));

  return router;
}

import {getSectors as getSectorsFromDb} from './db';

export function getSectors(deps) {
  const {database} = deps;

  return async (ctx) => {
    try {
      const {
        elementary,
      } = fp.getOr({}, 'req.query', ctx);


      const filter = elementary ?
        sector => sector('elementarySectors').count().eq(1) :
        () => true;

      ctx.body = await getSectorsFromDb(database, filter);
    } catch(err) {
      ctx.throw(500, err);
    }
  };
}
