import {} from 'dotenv/config';

import Koa from 'koa';
import cors from 'kcors';
import Router from 'koa-router';
import bodyParser from 'koa-bodyparser';
import createIoSocket from 'socket.io';

import d from 'debug';
const debug = d('4me.server');

import { connectToDb } from './rethink';
import { getRoutes } from './routes';
import { attachHandlers as attachSocketHandlers} from './socket';

(async function startUp() {
  // First try to contact database
  let database;
  try {
    database = await connectToDb();
  } catch(err) {
    process.exit(1);
  }

  const io = createIoSocket();
  attachSocketHandlers(io);

  const app = getApp({database, io});


  const port = parseInt(process.env.PORT) || 3100;
  const server = app.listen(port);
  io.attach(server);

  debug(`Server started, listening on port ${port}`);
})();

export function getApp(deps) {
  const app = new Koa();

  if(process.env.NODE_ENV === 'development') {
    const logger = require('koa-logger');
    app.use(logger());
  }

  app.use(async (ctx, next) => {
    const start = Date.now();
    await next();
    const delta = Math.ceil(Date.now() - start);
    ctx.set('X-Response-Time', `${delta}ms`);
  });

  // Error handling middleware
  app.use(async (ctx, next) => {
    try {
      await next();
    } catch(err) {
      ctx.status = err.status || 500;
      ctx.body = {
        status: err.status,
        cause: err.name,
        message: err.message,
      };
    };
  });

  app.use(cors());
  app.use(bodyParser());
  const router = getRoutes(new Router(), deps);

  app.use(router.routes());

  return app;
}


