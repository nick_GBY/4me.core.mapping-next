import installClientRoutes from './clients';
import installSectorRoutes from './sectors';
import installMapRoutes from './map';
import Router from 'koa-router'

export function getRoutes(mainRouter, deps = {}) {
  const clients = installClientRoutes(new Router(), deps);
  const sectors = installSectorRoutes(new Router(), deps);
  const map = installMapRoutes(new Router(), deps);

  mainRouter.use('/clients', clients.routes());
  mainRouter.use('/sectors', sectors.routes());
  mainRouter.use('/map', map.routes());

  return mainRouter;
}
